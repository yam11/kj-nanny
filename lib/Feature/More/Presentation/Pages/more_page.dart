import 'package:animate_do/animate_do.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:kjteacher/Feature/More/Presentation/Widgets/more_select_widget.dart';
import 'package:kjteacher/Feature/MyProfile/Presentation/Pages/my_profile_page.dart';
import 'package:kjteacher/Feature/Setting/Presentation/Pages/setting_page.dart';
import 'package:kjteacher/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:page_transition/page_transition.dart';

import 'package:sizer/sizer.dart';

class MorePage extends StatelessWidget {
  const MorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            FadeInLeft(
              duration: const Duration(milliseconds: 600),
              child: MoreSelectWidget(
                title: "My profile".tr(context),
                image: AppAssets.myProfileIcon,
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: TeacherProFile(teacherId: 1),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 300)));
                },
              ),
            ),
            FadeInLeft(
              duration: const Duration(milliseconds: 1400),
              child: MoreSelectWidget(
                title: "Settings".tr(context),
                image: AppAssets.settingsIcons,
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: SettingPage(),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 300)));
                },
              ),
            ),
            FadeInLeft(
              duration: const Duration(milliseconds: 1600),
              child: MoreSelectWidget(
                title: "Log out".tr(context),
                image: AppAssets.logoutIcon,
                onPressed: () {
                  AwesomeDialog(
                    title: "Are you sure".tr(context),
                    titleTextStyle: TextStyle(
                        color: AppColors.primaryColor,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold),
                    dialogBackgroundColor: Colors.white,
                    btnOkColor: AppColors.primaryColor,
                    btnCancelColor: AppColors.seconedaryColor,
                    btnOkText: "Yes".tr(context),
                    btnCancelText: "No".tr(context),
                    btnOkOnPress: () {
                      AppSharedPreferences.removeToken();
                      Navigator.of(context).pushAndRemoveUntil(
                          PageTransition(
                              child: SplashPage(),
                              type: PageTransitionType.rightToLeft,
                              duration: const Duration(milliseconds: 300)),
                          ModalRoute.withName("/"));
                    },
                    btnCancelOnPress: () {},
                    dismissOnTouchOutside: false,
                    context: context,
                    dialogType: DialogType.noHeader,
                    animType: AnimType.rightSlide,
                  ).show();
                },
              ),
            ),
            SizedBox(
              height: 4.h,
            )
          ],
        ),
      ),
    );
  }
}
