part of 'add_home_work_bloc.dart';

@immutable
abstract class AddHomeWorkEvent {}
class AddHomeWork extends AddHomeWorkEvent{}
