import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class TitleWidget extends StatelessWidget {
  String title;
  TitleWidget({required this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
      color: AppColors.whiteColor,
      child: Text(
        title,
        style: TextStyle(
            color: AppColors.primaryColor,
            fontSize: 16.sp,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
