import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:sizer/sizer.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 15.h,
        width: 15.w,
        child: const LoadingIndicator(
            indicatorType: Indicator.lineScaleParty,
            colors: [
              AppColors.primaryColor,
              AppColors.thirdColor,
              AppColors.grayColor,
              AppColors.primaryColor,
              AppColors.fourthColor,
              AppColors.grayColor
            ],
            strokeWidth: 5,
            backgroundColor: Colors.transparent,
            pathBackgroundColor: Colors.transparent),
      ),
    );
  }
}
