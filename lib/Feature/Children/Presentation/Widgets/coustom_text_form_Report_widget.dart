import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class CoustomTextFormFalidReport extends StatelessWidget {
  TextInputType textInputType;
  String title;

  bool enable;

  final TextEditingController controller;
  int maxLines;
  Function(String value) onChange;
  Function(String value) validator;

  final formKey;
  CoustomTextFormFalidReport(
      {required this.controller,
      required this.title,
      required this.enable,
      required this.formKey,
      required this.maxLines,
      required this.onChange,
      required this.textInputType,
      required this.validator,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.sp),
              ),
              SizedBox(
                width: 10.w,
              )
            ],
          ),
          Container(
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 2,
                  blurRadius: 4,
                  offset: const Offset(3, 3)),
            ]),
            child: TextFormField(
              validator: validator != null ? validator("") : null,
              enabled: enable,
              keyboardType: textInputType,
              controller: controller,
              maxLines: maxLines,
              cursorColor: AppColors.primaryColor,
              cursorHeight: 3.h,
              decoration: const InputDecoration(
                // prefixIconConstraints: BoxConstraints(maxWidth: 7.w),

                filled: true,

                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color.fromARGB(255, 242, 242, 242))),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.primaryColor),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red),
                ),
              ),
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14.0.sp,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
