import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/app_localizations.dart';

import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/error_message_widget.dart';
import 'package:kjteacher/Core/Widgets/loading_widget.dart';
import 'package:kjteacher/Feature/Children/Presentation/Pages/children_page.dart';
import 'package:kjteacher/Feature/Main/Bloc/bloc/children_teacher_bloc.dart';
import 'package:kjteacher/Feature/More/Presentation/Pages/more_page.dart';

import 'package:sizer/sizer.dart';

class MainPage extends StatelessWidget {
  MainPage({Key? key}) : super(key: key);
  ChildrenTeacherBloc childrenTeacherBloc = ChildrenTeacherBloc();
  // int initialIndex = 1;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.w),
                  bottomRight: Radius.circular(10.w)),
            ),
            backgroundColor: AppColors.primaryColor,
            title: Padding(
              padding: EdgeInsets.symmetric(vertical: 2.h),
              child: Text(
                "Teacher App".tr(context),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 30.sp,
                    fontWeight: FontWeight.bold,
                    color: AppColors.seconedaryColor),
              ),
            ),
            bottom: TabBar(
                padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
                // controller: tabController,
                isScrollable: false,
                labelColor: AppColors.primaryColor,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.w),
                  color: AppColors.whiteColor,
                ),
                splashBorderRadius: BorderRadius.circular(10.w),
                labelStyle:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                // indicatorColor: Colors.transparent,
                unselectedLabelColor: AppColors.whiteColor,
                tabs: [
                  Tab(
                    text: "Children".tr(context),
                  ),
                  Tab(
                    text: "More".tr(context),
                  ),
                ]),
          ),
          body: BackGraoundWidget(
              child: BlocProvider(
            create: (context) => childrenTeacherBloc..add(ChildrenTeacher()),
            child: BlocConsumer<ChildrenTeacherBloc, ChildrenTeacherState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is ErrorToGetChildrenTeacher) {
                  return ErrorMessageWidget(
                      onPressed: () {}, message: state.message);
                }
                if (state is SuccesToGetChildrenTeacher) {
                  return TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    // controller: tabController,
                    children: [
                      ChildrenPage(
                        childrenTeacherBloc: childrenTeacherBloc,
                      ),
                      MorePage(),
                    ],
                  );
                } else {
                  return const LoadingWidget();
                }
              },
            ),
          ))),
    );
  }
}
