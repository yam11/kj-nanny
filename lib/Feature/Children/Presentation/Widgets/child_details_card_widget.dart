import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ChildDetailsCardWidget extends StatelessWidget {
  String cardImage;
  String cardTitle;
  final onTap;
  ChildDetailsCardWidget(
      {required this.onTap,
      required this.cardImage,
      required this.cardTitle,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25.h,
      margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
      decoration: const BoxDecoration(color: AppColors.whiteColor, boxShadow: [
        BoxShadow(
          color: AppColors.grayColor,
          spreadRadius: 1,
          blurRadius: 3,
          offset: Offset(0, 3),
        ),
      ]),
      child: InkWell(
        onTap: onTap,
        child: Column(children: [
          Container(
            color: AppColors.seconedaryColor.withOpacity(0.4),
            padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 2.h),
            alignment: Alignment.center,
            child: Text(cardTitle,
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold)),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(cardImage)),
                color: AppColors.whiteColor,
              ),
            ),
          )
        ]),
      ),
    );
  }
}
