import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/tilte_page_widget.dart';
import 'package:kjteacher/Feature/Children/Bloc/add_reprot_bloc/add_reprot_bloc.dart';
import 'package:kjteacher/Feature/Children/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:kjteacher/Feature/Children/Models/report_model.dart';
import 'package:kjteacher/Feature/Children/Presentation/Widgets/child_report_widget.dart';
import 'package:kjteacher/Feature/Children/Presentation/Widgets/coustom_text_form_Report_widget.dart';
import 'package:sizer/sizer.dart';
import 'package:wc_form_validators/wc_form_validators.dart';

class AddReportPage extends StatelessWidget {
  ChildrenProfileBloc childrenProfileBloc;
  AddReportPage({required this.childrenProfileBloc, Key? key})
      : super(key: key);
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  AddReprotBloc addReprotBloc = AddReprotBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
        child: SafeArea(
          child: BlocProvider(
            create: (context) => addReprotBloc,
            child: Column(children: [
              FadeInDown(
                  duration: const Duration(milliseconds: 1600),
                  child: TitlePageWidget(title: "Add Reports".tr(context))),
              BlocConsumer<AddReprotBloc, AddReprotState>(
                listener: (context, state) {
                  if (state is SuccesAddReprotl) {
                    addReprotBloc.noteController.clear();
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: const Duration(seconds: 2),
                      content: Row(
                        children: [
                          Text(
                            "Succes To Add Reprot",
                            style: TextStyle(
                                color: AppColors.whiteColor, fontSize: 11.sp),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                        ],
                      ),
                      backgroundColor: (Colors.green),
                    ));
                  }
                },
                builder: (context, state) {
                  return Expanded(
                      child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            CoustomTextFormFalidReport(
                              title: "Note".tr(context),
                              controller: addReprotBloc.noteController,
                              enable: true,
                              formKey: null,
                              maxLines: 4,
                              onChange: (_) {},
                              textInputType: TextInputType.text,
                              validator: (value) => Validators.compose([
                                Validators.required("empty"),
                              ]),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            state is! LoadingAddReprotl
                                ? MaterialButton(
                                    color: AppColors.primaryColor,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 2.w, vertical: 1.h),
                                    onPressed: () {
                                      if (formKey.currentState!.validate()) {
                                        addReprotBloc.add(AddReprot());
                                      }
                                    },
                                    child: Text(
                                      "Send".tr(context),
                                      style: TextStyle(
                                          color: AppColors.seconedaryColor,
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )
                                : const CircularProgressIndicator(
                                    color: AppColors.primaryColor,
                                  ),
                            SizedBox(
                              height: 2.h,
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: note.length,
                              itemBuilder: (context, index) {
                                return ChildReportWidget(
                                  hour: note[index].hour,
                                  note: note[index].note,
                                );
                              },
                            )
                          ],
                        )),
                  ));
                },
              )
            ]),
          ),
        ),
      ),
    );
  }
}
