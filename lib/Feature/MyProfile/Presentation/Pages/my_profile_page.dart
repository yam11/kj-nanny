import 'package:animate_do/animate_do.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Feature/Comments/Models/comments_model.dart';
import 'package:kjteacher/Feature/Comments/Presentation/Pages/commenst_page.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/error_message_widget.dart';
import 'package:kjteacher/Core/Widgets/loading_widget.dart';
import 'package:kjteacher/Core/Widgets/tilte_page_widget.dart';
import 'package:kjteacher/Core/Widgets/title_widget.dart';

import 'package:kjteacher/Core/Widgets/carousel_slider_image.dart';
import 'package:kjteacher/Feature/MyProfile/Bloc/bloc/teacher_bloc.dart';
import 'package:kjteacher/Feature/MyProfile/Widgets/teacher_profile_widget.dart';

import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class TeacherProFile extends StatefulWidget {
  int teacherId;
  TeacherProFile({required this.teacherId, Key? key}) : super(key: key);

  @override
  State<TeacherProFile> createState() => _TeacherProFileState();
}

class _TeacherProFileState extends State<TeacherProFile> {
  final controller = CarouselController();
  int activeIndex = 0;
  TeacherBloc teacherBloc = TeacherBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: Column(
          children: [
            FadeInDown(
                duration: const Duration(milliseconds: 1600),
                child: TitlePageWidget(title: "My Profile".tr(context))),
            Expanded(
              child: BlocProvider(
                create: (context) => teacherBloc
                  ..add(GetTeacherProFile(teacherId: widget.teacherId)),
                child: BlocConsumer<TeacherBloc, TeacherState>(
                  listener: (context, state) {
                    // TODO: implement listener
                  },
                  builder: (context, state) {
                    if (state is ErrorToGetTeacherProfile) {
                      return ErrorMessageWidget(
                          onPressed: () {
                            teacherBloc.add(
                                GetTeacherProFile(teacherId: widget.teacherId));
                          },
                          message: state.message);
                    }
                    if (state is SuccesToGetTeacherProfile) {
                      return ListView(
                        physics: const BouncingScrollPhysics(),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                radius: 10.w,
                                backgroundColor: AppColors.primaryColor,
                                backgroundImage:
                                    const AssetImage(AppAssets.womanIcon),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.h,
                          ),
                          TeacherProfileWidget(
                              teacherModel: teacherBloc.teacherModel!),
                          SizedBox(
                            height: 3.h,
                          ),
                          TitleWidget(
                              title: "Scientific certificates obtained"
                                  .tr(context)),
                          SizedBox(
                            height: 2.h,
                          ),
                          Container(
                            height: 30.h,
                            margin: EdgeInsets.symmetric(horizontal: 5.w),
                            decoration: BoxDecoration(
                                color: AppColors.whiteColor,
                                border: Border.all(
                                    color: AppColors.grayColor, width: 2),
                                borderRadius: BorderRadius.circular(5.w),
                                image: DecorationImage(
                                    image: NetworkImage(teacherBloc
                                        .certificateModel!.data!.certificate!),
                                    fit: BoxFit.fill)),
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              MaterialButton(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 2.w, vertical: 2.h),
                                  color: AppColors.primaryColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.w)),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      PageTransition(
                                          child: CommentsPage(
                                              commendModel: commendForNanny),
                                          type: PageTransitionType.rightToLeft,
                                          duration: const Duration(
                                              milliseconds: 300)),
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        "Opinion about me".tr(context),
                                        style: TextStyle(
                                            color: AppColors.seconedaryColor,
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                          SizedBox(
                            height: 5.h,
                          )
                        ],
                      );
                    } else {
                      return const LoadingWidget();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
