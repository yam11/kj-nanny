class ChildTeacherModel {
  Data? data;

  ChildTeacherModel({this.data});

  ChildTeacherModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? firstName;
  String? lastName;
  String? birthDate;
  String? address;
  String? phoneNumber;
  Classroom? classroom;
  List<Children>? children;

  Data(
      {this.id,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.address,
      this.phoneNumber,
      this.classroom,
      this.children});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    birthDate = json['birthDate'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    classroom = json['classroom'] != null
        ? new Classroom.fromJson(json['classroom'])
        : null;
    if (json['children'] != null) {
      children = <Children>[];
      json['children'].forEach((v) {
        children!.add(new Children.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['birthDate'] = this.birthDate;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    if (this.classroom != null) {
      data['classroom'] = this.classroom!.toJson();
    }
    if (this.children != null) {
      data['children'] = this.children!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Classroom {
  int? id;
  int? teacherId;
  String? level;

  Classroom({this.id, this.teacherId, this.level});

  Classroom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    teacherId = json['teacherId'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['teacherId'] = this.teacherId;
    data['level'] = this.level;
    return data;
  }
}

class Children {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  int? busLineId;
  String? status;
  String? birthDate;

  Children(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.busLineId,
      this.status,
      this.birthDate});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    busLineId = json['busLineId'];
    status = json['status'];
    birthDate = json['birthDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['busLineId'] = this.busLineId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    return data;
  }
}
