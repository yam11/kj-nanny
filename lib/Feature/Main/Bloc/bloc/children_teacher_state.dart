part of 'children_teacher_bloc.dart';

@immutable
abstract class ChildrenTeacherState {}

class ChildrenTeacherInitial extends ChildrenTeacherState {}
class SuccesToGetChildrenTeacher extends ChildrenTeacherState {}
class ErrorToGetChildrenTeacher extends ChildrenTeacherState {
  final String message;
  ErrorToGetChildrenTeacher({required this. message});
}
class LoadingToGetChildrenTeacher extends ChildrenTeacherState {}
