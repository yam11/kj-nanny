part of 'add_reprot_bloc.dart';

@immutable
abstract class AddReprotState {}

class AddReprotInitial extends AddReprotState {}

class LoadingAddReprotl extends AddReprotState {}

class SuccesAddReprotl extends AddReprotState {}
