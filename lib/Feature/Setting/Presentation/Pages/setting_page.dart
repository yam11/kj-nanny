import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/Bloc/app_language_cubit/app_language_cubit.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/tilte_page_widget.dart';
import 'package:kjteacher/Feature/Setting/Models/locale_keys.g.dart';
import 'package:kjteacher/Feature/Setting/Presentation/Widgets/language_button_widget.dart';
import 'package:sizer/sizer.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: BlocConsumer<AppLanguageCubit, ChangeLanguage>(
          listener: (context, state) {},
          builder: (context, state) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Language".tr(context),
                        style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          LanguageButton(
                            title: 'English'.tr(context),
                            onPress: () {
                              context
                                  .read<AppLanguageCubit>()
                                  .changeLanguageToEn();
                            },
                            isPressed: !AppSharedPreferences.hasArLang,
                          ),
                          LanguageButton(
                            title: 'Arabic'.tr(context),
                            onPress: () {
                              context
                                  .read<AppLanguageCubit>()
                                  .changeLanguageToAr();
                            },
                            isPressed: AppSharedPreferences.hasArLang,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      )),
    );
  }
}
