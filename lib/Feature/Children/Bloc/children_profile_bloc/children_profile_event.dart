part of 'children_profile_bloc.dart';

@immutable
abstract class ChildrenProfileEvent {}

class ViewOrders extends ChildrenProfileEvent {
  final int orderId;
  ViewOrders({required this.orderId});
}

class ChildrenDetails extends ChildrenProfileEvent {
  final int childId;
  ChildrenDetails({required this.childId});
}

class RemoveOrder extends ChildrenProfileEvent {
  final int orderId;
  int index;
  RemoveOrder({required this.orderId, required this.index});
}
