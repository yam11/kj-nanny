import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ErrorMessageWidget extends StatelessWidget {
  const ErrorMessageWidget(
      {required this.onPressed, required this.message, Key? key})
      : super(key: key);
  final String message;
  final onPressed;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Align(
            alignment: Alignment.center,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 2.w),
              decoration: BoxDecoration(
                  color: AppColors.seconedaryColor,
                  borderRadius: BorderRadius.circular(5.w)),
              child: Column(children: [
                Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: AppColors.primaryColor,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold),
                ),
                MaterialButton(
                  onPressed: onPressed,
                  color: AppColors.primaryColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.w)),
                  child: Text(
                    "Try again".tr(context),
                    style: TextStyle(
                        color: AppColors.seconedaryColor,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ]),
            )),
      ],
    );
  }
}
