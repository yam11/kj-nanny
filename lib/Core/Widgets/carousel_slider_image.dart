import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';

import 'package:sizer/sizer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

Widget buildImage(List urlImage, int index) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 5.w),
    decoration: BoxDecoration(
        color: AppColors.whiteColor,
        border: Border.all(color: AppColors.grayColor, width: 2),
        borderRadius: BorderRadius.circular(5.w),
        image: DecorationImage(
            image: NetworkImage(urlImage[index].image!), fit: BoxFit.fill)),
  );
}

Widget buildIndicator(int index, int count) => AnimatedSmoothIndicator(
    activeIndex: index,
    count: count,
    effect: const SlideEffect(
        spacing: 8.0,
        radius: 15.0,
        dotWidth: 10.0,
        dotHeight: 10.0,
        paintStyle: PaintingStyle.stroke,
        strokeWidth: 1.5,
        dotColor: AppColors.seconedaryColor,
        activeDotColor: AppColors.primaryColor));
