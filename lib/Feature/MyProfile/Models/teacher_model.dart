class TeacherModel {
  Data? data;

  TeacherModel({this.data});

  TeacherModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? firstName;
  String? lastName;
  String? birthDate;
  String? address;
  String? phoneNumber;
  Classroom? classroom;

  Data(
      {this.id,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.address,
      this.phoneNumber,
      this.classroom});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    birthDate = json['birthDate'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    classroom = json['classroom'] != null
        ? new Classroom.fromJson(json['classroom'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['birthDate'] = this.birthDate;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    if (this.classroom != null) {
      data['classroom'] = this.classroom!.toJson();
    }
    return data;
  }
}

class Classroom {
  int? id;
  int? teacherId;
  String? level;

  Classroom({this.id, this.teacherId, this.level});

  Classroom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    teacherId = json['teacherId'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['teacherId'] = this.teacherId;
    data['level'] = this.level;
    return data;
  }
}
