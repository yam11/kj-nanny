import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjteacher/Core/Api/ExceptionsHandle.dart';
import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Api/Urls.dart';

import 'package:meta/meta.dart';

part 'change_state_event.dart';
part 'change_state_state.dart';

class ChangeStateBloc extends Bloc<ChangeStateEvent, ChangeStateState> {
  ChangeStateBloc() : super(ChangeStateInitial()) {
    on<ChangeStateEvent>((event, emit) async {
      if (event is ChangeState) {
        emit(LoadingToChangeState());

        try {
          await Network.patchData(
              url: "${Urls.changeState}${event.childId}",
              data: {"status": event.state});

          emit(SuccessToChangeState());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToChangeState(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToChangeState(message: "error"));
          }
        }
      }
    });
  }
}
