import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';
// import 'package:sizer/sizer.dart';

class CoustomTextFotmFalid extends StatefulWidget {
  TextInputType textInputType;
  bool isPassword;

  // bool enable;

  final TextEditingController controller;
  int maxLines;
  Function(String value) onChange;
  Function(String value) validator;

  final formKey;
  CoustomTextFotmFalid(
      {required this.controller,
      required this.formKey,
      required this.isPassword,
      required this.maxLines,
      required this.onChange,
      required this.textInputType,
      required this.validator,
      Key? key})
      : super(key: key);

  @override
  State<CoustomTextFotmFalid> createState() => _CoustomTextFotmFalidState();
}

class _CoustomTextFotmFalidState extends State<CoustomTextFotmFalid> {
  bool _showPassword = false;
  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5.w),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(3, 3)),
      ]),
      child: TextFormField(
        
        validator: widget.validator != null ? widget.validator("") : null,
        // enabled: widget.enable,
        obscureText: !widget.isPassword ? _showPassword : !_showPassword,
        keyboardType: widget.textInputType,
        controller: widget.controller,
        maxLines: widget.maxLines,
        cursorColor: AppColors.primaryColor,
        cursorHeight: 3.h,
        decoration: InputDecoration(
          suffixIcon: !widget.isPassword
              ? null
              : IconButton(
                  onPressed: () {
                    _togglevisibility();
                  },
                  icon: Icon(
                    _showPassword
                        ? Icons.visibility_outlined
                        : Icons.visibility_off_outlined,
                    color: AppColors.primaryColor,
                  ),
                ),
          filled: true,
          fillColor: Colors.white,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.w),
              borderSide: const BorderSide(color: Colors.white)),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.w),
            borderSide: const BorderSide(color: AppColors.primaryColor),
          ),
          errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.w),
              borderSide: const BorderSide(color: Colors.red)),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.w),
              borderSide: const BorderSide(color: AppColors.primaryColor)),
        ),

        style: TextStyle(
          fontSize: 14.sp,
          color: AppColors.primaryColor,
        ),
      ),
    );
  }
}
