class ChildDetailsModel {
  Data? data;

  ChildDetailsModel({this.data});

  ChildDetailsModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  int? busLineId;
  String? status;
  String? birthDate;
  Guardian? guardian;
  Classroom? classroom;
  Image? image;
  List<Orders>? orders;
  List<Reports>? reports;

  Data(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.busLineId,
      this.status,
      this.birthDate,
      this.guardian,
      this.classroom,
      this.image,
      this.orders,
      this.reports});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    busLineId = json['busLineId'];
    status = json['status'];
    birthDate = json['birthDate'];
    guardian = json['guardian'] != null
        ? new Guardian.fromJson(json['guardian'])
        : null;
    classroom = json['classroom'] != null
        ? new Classroom.fromJson(json['classroom'])
        : null;
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    if (json['orders'] != null) {
      orders = <Orders>[];
      json['orders'].forEach((v) {
        orders!.add(new Orders.fromJson(v));
      });
    }
    if (json['reports'] != null) {
      reports = <Reports>[];
      json['reports'].forEach((v) {
        reports!.add(new Reports.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['busLineId'] = this.busLineId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    if (this.guardian != null) {
      data['guardian'] = this.guardian!.toJson();
    }
    if (this.classroom != null) {
      data['classroom'] = this.classroom!.toJson();
    }
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    if (this.orders != null) {
      data['orders'] = this.orders!.map((v) => v.toJson()).toList();
    }
    if (this.reports != null) {
      data['reports'] = this.reports!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Guardian {
  int? id;
  String? firstName;
  String? lastName;
  String? address;
  String? phoneNumber;
  int? balance;

  Guardian(
      {this.id,
      this.firstName,
      this.lastName,
      this.address,
      this.phoneNumber,
      this.balance});

  Guardian.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['LastName'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    balance = json['balance'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    data['balance'] = this.balance;
    return data;
  }
}

class Classroom {
  int? id;
  int? teacherId;
  String? level;

  Classroom({this.id, this.teacherId, this.level});

  Classroom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    teacherId = json['teacherId'];
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['teacherId'] = this.teacherId;
    data['level'] = this.level;
    return data;
  }
}

class Image {
  int? id;
  int? childId;
  String? image;

  Image({this.id, this.childId, this.image});

  Image.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    childId = json['childId'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['childId'] = this.childId;
    data['image'] = this.image;
    return data;
  }
}

class Orders {
  int? id;
  int? childId;
  int? totalPrice;
  String? createdAt;

  Orders({this.id, this.childId, this.totalPrice, this.createdAt});

  Orders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    childId = json['childId'];
    totalPrice = json['totalPrice'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['childId'] = this.childId;
    data['totalPrice'] = this.totalPrice;
    data['createdAt'] = this.createdAt;
    return data;
  }
}

class Reports {
  int? childId;
  String? description;
  String? createdAt;

  Reports({this.childId, this.description, this.createdAt});

  Reports.fromJson(Map<String, dynamic> json) {
    childId = json['childId'];
    description = json['description'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['childId'] = this.childId;
    data['description'] = this.description;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
