class AppAssets {
  /*images*/
  static const String homeBackGraound = "assets/images/Edges.png";
  static const String yallow = "assets/images/yallow.png";
  static const String purple = "assets/images/purple.png";
  static const String blue = "assets/images/blue.png";
  static const String motherImage = "assets/images/mother.png";
  static const String studentImage = "assets/images/student.png";
  static const String kidImage = "assets/images/kid.png";
  static const String pizzaImage = "assets/images/pizza.png";
  static const String lemonImage = "assets/images/lemon.png";
  static const String guysImage = "assets/images/pexels.png";
  static const String nannyImage = "assets/images/nanny.png";
  static const String redNoteImage = "assets/images/notered.png";
  static const String blueNoteImage = "assets/images/noteblue.png";
  /*icons*/
  static const String foodIcon = "assets/icons/dinner.png";
  static const String profileIcon = "assets/icons/user.png";
  static const String settingsIcons = "assets/icons/settings.png";
  static const String logoutIcon = "assets/icons/exit.png";
  static const String myProfileIcon = "assets/icons/myprofile.png";
  static const String teacherIcon = "assets/icons/teacher.png";
  static const String womanIcon = "assets/icons/woman.png";
  static const String reportIcon = "assets/icons/report.png";
  static const String resultsIcon = "assets/icons/results.png";
  static const String homeworkIcon = "assets/icons/homework.png";
}
