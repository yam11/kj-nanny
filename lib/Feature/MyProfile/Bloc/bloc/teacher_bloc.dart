import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjteacher/Core/Api/ExceptionsHandle.dart';
import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Api/Urls.dart';
import 'package:kjteacher/Feature/MyProfile/Models/certificate_model.dart';
import 'package:kjteacher/Feature/MyProfile/Models/teacher_model.dart';

import 'package:meta/meta.dart';

part 'teacher_event.dart';
part 'teacher_state.dart';

class TeacherBloc extends Bloc<TeacherEvent, TeacherState> {
  
  TeacherModel? teacherModel;
  CertificateModel? certificateModel;
  TeacherBloc() : super(TeacherInitial()) {
    on<TeacherEvent>((event, emit) async {
    
      if (event is GetTeacherProFile) {
        emit(LoadingToGetTeacherProfile());
        try {
          final response1 = await Network.getData(
              url: "${Urls.teachersProfile}${event.teacherId}");
          final response2 = await Network.getData(
              url: "${Urls.teacherCertificates}${event.teacherId}");

          teacherModel = TeacherModel.fromJson(response1.data);
          certificateModel = CertificateModel.fromJson(response2.data);
          emit(SuccesToGetTeacherProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetTeacherProfile(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetTeacherProfile(message: "error"));
          }
        }
      }
    });
  }
}
