import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Feature/Comments/Models/comments_model.dart';
import 'package:kjteacher/Feature/Comments/Presentation/Widget/comments_widget.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/loading_widget.dart';
import 'package:kjteacher/Core/Widgets/tilte_page_widget.dart';

class CommentsPage extends StatefulWidget {
  List<CommentsModel> commendModel;
  CommentsPage({required this.commendModel, Key? key}) : super(key: key);

  @override
  State<CommentsPage> createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  @override
  void initState() {
    super.initState();
     loadingState();
  }

  bool loading = false;

  loadingState() async {
    await Future.delayed(const Duration(seconds: 1));
    setState(() {
      loading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(AppBar().preferredSize.height),
          child: const AppBarWidget(),
        ),
        body: BackGraoundWidget(
            child: SafeArea(
                child: Column(
          children: [
            FadeInDown(
                duration: const Duration(milliseconds: 1600),
                child: TitlePageWidget(title: "Opinion about me".tr(context))),
            Expanded(
              child: loading? AnimationLimiter(
                child: ListView.builder(
                  physics: const BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  itemCount: widget.commendModel.length,
                  itemBuilder: (context, index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      delay: const Duration(milliseconds: 600),
                      child: SlideAnimation(
                        duration: const Duration(milliseconds: 600),
                        curve: Curves.fastLinearToSlowEaseIn,
                        horizontalOffset: -300,
                        verticalOffset: -850,
                        child: CommentsWidget(
                          avatarImage: AppAssets.womanIcon,
                          commendModel: widget.commendModel,
                          index: index,
                        ),
                      ),
                    );
                  },
                ),
              ):const  LoadingWidget(),
            ),
          ],
        ))));
  }
}
