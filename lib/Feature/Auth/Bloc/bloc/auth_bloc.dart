import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjteacher/Core/Api/ExceptionsHandle.dart';

import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Api/Urls.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      if (event is LogIn) {
        final formData = FormData.fromMap(
            {"email": event.email, "password": event.password});
        emit(LoadingToLogIn());
        try {
          final response =
              await Network.postData(url: Urls.loginApi, data: formData);
          AppSharedPreferences.saveToken(response.data['token']);
          AppSharedPreferences.saveId(response.data['user']['id']);

          emit(SuccesToLogIn());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToLogIn(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToLogIn(message: "error"));
          }
        }
      }
    });
  }
}
