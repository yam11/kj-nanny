import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';

import 'package:sizer/sizer.dart';

class ChildFoodWidget extends StatelessWidget {
  String name;
  String decoration;
  int index;

  ChildFoodWidget(
      {required this.index,
      required this.name,
      required this.decoration,
      Key? key})
      : super(key: key);
  List<String> images = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuif3zv_toPxMGoEYD8le63K-Thc55RuP01w&usqp=CAU",
    "https://realfood.tesco.com/media/images/RFO-Main-472x310-QuinoaNuggets-fc2ba00e-5587-4075-923c-363a93024094-0-472x310.jpg"
  ];
  List<String> foodName = ["Jerrod Jones", "Mrs. Lorine Haag DVM"];
  List<String> decorationFood = [
    "Excepturi voluptatem voluptatem consequatur autem deleniti aut. ",
    "Est odit ipsam illo possimus facere voluptas. Et eos consequuntur earum tenetur nisi eaque. Quo voluptas laboriosam suscipit doloribus."
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
      decoration: BoxDecoration(
          color: AppColors.primaryColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "Name".tr(context),
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: Text(
                  foodName[index],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                "Description".tr(context),
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  decorationFood[index],
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  height: 18.h,
                  padding: EdgeInsets.symmetric(horizontal: 2.w),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.fill, image: NetworkImage(images[index])),
                    color: AppColors.grayColor.withOpacity(0.5),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "2023-02-24 | 13:50:22",
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.bold),
              )
            ],
          )
        ],
      ),
    );
  }
}
