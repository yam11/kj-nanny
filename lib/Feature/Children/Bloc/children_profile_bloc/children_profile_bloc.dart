import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjteacher/Core/Api/ExceptionsHandle.dart';
import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Api/Urls.dart';
import 'package:kjteacher/Feature/Children/Models/childern_details_model.dart';
import 'package:kjteacher/Feature/Children/Models/view_order_model.dart';

import 'package:meta/meta.dart';

part 'children_profile_event.dart';
part 'children_profile_state.dart';

class ChildrenProfileBloc
    extends Bloc<ChildrenProfileEvent, ChildrenProfileState> {
  ViewOrderModel? viewOrderModel;
  ChildDetailsModel? childDetailsModel;
  List<Orders> order = [];
  ChildrenProfileBloc() : super(ChildrenProfileInitial()) {
    on<ChildrenProfileEvent>((event, emit) async {
      if (event is ViewOrders) {
        emit(LoadingToViewOrder());
        try {
          final response =
              await Network.getData(url: "${Urls.viewOrder} ${event.orderId}");
          // viewOrderModel = ViewOrderModel.fromJson(response.data);
          emit(SuccesToViewOrder());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToViewOrder(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToViewOrder(message: "error"));
          }
        }
      }
      if (event is ChildrenDetails) {
        emit(LoadingToGetChildrenDetails());
        try {
          final response = await Network.getData(
              url: "${Urls.childDetails} ${event.childId}");
          childDetailsModel = ChildDetailsModel.fromJson(response.data);
          order.addAll(childDetailsModel!.data!.orders!);
          emit(SuccesToGetChildrenDetails());
        } catch (error) {
          print("whaat $error");
          if (error is DioError) {
            emit(ErrorToGetChildrenDetails(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenDetails(message: "error"));
          }
        }
      }
      if (event is RemoveOrder) {
        emit(LoadingToDeleteOrder());
        try {
          await Network.deleteData(url: "${Urls.viewOrder}${event.orderId}");
          order.removeAt(event.index);
          emit(SuccesToDeleteOrder());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenDetails(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenDetails(message: "error"));
          }
        }
      }
    });
  }
}
