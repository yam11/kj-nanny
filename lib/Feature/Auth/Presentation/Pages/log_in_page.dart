import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Feature/Auth/Bloc/bloc/auth_bloc.dart';
import 'package:kjteacher/Feature/Auth/Presentation/Widgets/coustom_text_falid.dart';
import 'package:kjteacher/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';
import 'package:wc_form_validators/wc_form_validators.dart';

class LogInPage extends StatelessWidget {
  LogInPage({Key? key}) : super(key: key);

  TextEditingController userNameController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  AuthBloc authBloc = AuthBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BackGraoundWidget(
        child: BlocProvider(
          create: (context) => authBloc,
          child: SafeArea(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FadeInLeft(
                    duration: const Duration(milliseconds: 1600),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 5.w,
                        ),
                        Text(
                          "login".tr(context),
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontSize: 40.sp,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FadeInLeft(
                        duration: const Duration(milliseconds: 1600),
                        child: Container(
                          height: 30.h,
                          width: 25.w,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(AppAssets.nannyImage),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                      FadeInRight(
                        duration: const Duration(milliseconds: 1600),
                        child: Container(
                          height: 30.h,
                          width: 30.w,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(AppAssets.studentImage),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ],
                  ),
                  Form(
                      key: formKey,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 1.h,
                          ),
                          FadeInLeft(
                            duration: const Duration(milliseconds: 1600),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 10.w,
                                ),
                                Text(
                                  "UserName".tr(context),
                                  style: TextStyle(
                                      color: AppColors.primaryColor,
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          CoustomTextFotmFalid(
                            controller: userNameController,
                            isPassword: false,
                            maxLines: 1,
                            textInputType: TextInputType.name,
                            onChange: (value) {},
                            validator: (value) => Validators.compose([
                              Validators.required(
                                  "Enter your UserName".tr(context)),
                            ]),
                            formKey: null,
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          FadeInLeft(
                            duration: const Duration(milliseconds: 1600),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 10.w,
                                ),
                                Text(
                                  "Password".tr(context),
                                  style: TextStyle(
                                      color: AppColors.primaryColor,
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          CoustomTextFotmFalid(
                            controller: passwordController,
                            isPassword: true,
                            maxLines: 1,
                            textInputType: TextInputType.name,
                            onChange: (value) {},
                            validator: (value) => Validators.compose([
                              Validators.required(
                                  "Enter your password".tr(context)),
                            ]),
                            formKey: null,
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          FadeInUp(
                            duration: const Duration(milliseconds: 1600),
                            child: BlocConsumer<AuthBloc, AuthState>(
                              listener: (context, state) {
                                if (state is SuccesToLogIn) {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    duration: const Duration(seconds: 2),
                                    content: Row(
                                      children: [
                                        Text(
                                          "LogIn Succes",
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: 11.sp),
                                        ),
                                        SizedBox(
                                          width: 4.w,
                                        ),
                                      ],
                                    ),
                                    backgroundColor: (Colors.green),
                                  ));
                                  Navigator.pushReplacement(
                                      context,
                                      PageTransition(
                                          child: MainPage(),
                                          type: PageTransitionType.rightToLeft,
                                          duration: const Duration(
                                              milliseconds: 300)));
                                }
                                if (state is ErrorToLogIn) {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    duration: const Duration(seconds: 2),
                                    content: Row(
                                      children: [
                                        Text(
                                          "LogIn Falid",
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: 11.sp),
                                        ),
                                        SizedBox(
                                          width: 4.w,
                                        ),
                                      ],
                                    ),
                                    backgroundColor: (Colors.red),
                                  ));
                                }
                              },
                              builder: (context, state) {
                                return state is! LoadingToLogIn
                                    ? MaterialButton(
                                        onPressed: () {
                                          if (formKey.currentState!
                                              .validate()) {
                                            authBloc.add(LogIn(
                                                email: userNameController.text,
                                                password:
                                                    passwordController.text));
                                          }
                                        },
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 5.w, vertical: 2.h),
                                        color: AppColors.primaryColor,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.w)),
                                        child: Text(
                                          "Log in".tr(context),
                                          style: TextStyle(
                                              color: AppColors.whiteColor,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    : const Center(
                                        child: CircularProgressIndicator(
                                          color: AppColors.primaryColor,
                                        ),
                                      );
                              },
                            ),
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
