import 'package:flutter/material.dart';

import 'package:sizer/sizer.dart';

import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';

class ChildHomeWorkWidget extends StatelessWidget {
  String name;

  String decoration;
  ChildHomeWorkWidget({required this.decoration, required this.name, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      decoration: BoxDecoration(
          color: AppColors.fourthColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Column(children: [
        Row(
          children: [
            Text(
              "Name".tr(context),
              style: TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 8.w,
            ),
            Expanded(
              child: Text(
                name,
                style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          children: [
            Text(
              "Decoration".tr(context),
              style: TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                decoration,
                style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        )
      ]),
    );
  }
}
