import 'package:flutter/material.dart';

import 'package:sizer/sizer.dart';

import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';

class ChildReportWidget extends StatelessWidget {
  String hour;

  String note;
  ChildReportWidget({required this.note, required this.hour, Key? key})
      : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      decoration: BoxDecoration(
          color: AppColors.fourthColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Column(children: [
        Row(
          children: [
            Text(
              "Data".tr(context),
              style: TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 8.w,
            ),
            Expanded(
              child: Text(
                hour,
                style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          children: [
            Text(
              "Note".tr(context),
              style: TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          children: [
            Expanded(
              child: Text(
                note,
                style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        )
      ]),
    );
  }
}
