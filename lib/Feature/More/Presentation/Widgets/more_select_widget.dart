import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class MoreSelectWidget extends StatelessWidget {
  String title;
  String image ;
  final onPressed;
  MoreSelectWidget({required this.image,required this.onPressed, required this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 5.w,
        vertical: 3.h,
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(3, 3)),
      ]),
      child: MaterialButton(
        onPressed: onPressed,
        padding: EdgeInsets.symmetric(
          horizontal: 2.w,
          vertical: 3.h,
        ),
        minWidth: double.infinity,
        color: AppColors.whiteColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.w),
        ),
        child: Row(
          children: [
            Container(
              height: 8.h,
              width: 16.w,
              decoration:  BoxDecoration(
                  image:
                      DecorationImage(image: AssetImage(image))),
            ),
            SizedBox(
              width: 5.w,
            ),
            Text(
              title,
              style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.bold,
                  color: AppColors.primaryColor),
            ),
          ],
        ),
      ),
    );
  }
}
