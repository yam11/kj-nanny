part of 'change_state_bloc.dart';

@immutable
abstract class ChangeStateState {}

class ChangeStateInitial extends ChangeStateState {}

class SuccessToChangeState extends ChangeStateState {}

class ErrorToChangeState extends ChangeStateState {
  final String message;
  ErrorToChangeState({required this.message});
}

class LoadingToChangeState extends ChangeStateState {}
