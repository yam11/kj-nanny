import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:kjteacher/Feature/Children/Models/report_model.dart';
import 'package:meta/meta.dart';

part 'add_reprot_event.dart';
part 'add_reprot_state.dart';

class AddReprotBloc extends Bloc<AddReprotEvent, AddReprotState> {
  TextEditingController noteController = TextEditingController();
  AddReprotBloc() : super(AddReprotInitial()) {
    on<AddReprotEvent>((event, emit) async {
      if (event is AddReprot) {
        emit(LoadingAddReprotl());
        await Future.delayed(const Duration(seconds: 1));
        note.add(NoteModel(hour: "2023-02-24 | 23:00:22", note: noteController.text));
        emit(SuccesAddReprotl());
      }
    });
  }
}
