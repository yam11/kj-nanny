import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:kjteacher/Feature/Auth/Presentation/Pages/log_in_page.dart';
import 'package:kjteacher/Feature/Main/Presentation/Pages/main_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool animate = false;
  @override
  void initState() {
    startAnimation();

    Timer(const Duration(seconds: 4), () {
      if (!AppSharedPreferences.hasToken) {
        Navigator.pushReplacement(
            context,
            PageTransition(
                child: LogInPage(),
                type: PageTransitionType.rightToLeft,
                duration: const Duration(milliseconds: 300)));
      } else {
        Navigator.pushReplacement(
            context,
            PageTransition(
                child: MainPage(),
                type: PageTransitionType.rightToLeft,
                duration: const Duration(milliseconds: 300)));
      }
    });
    super.initState();
  }

  Future startAnimation() async {
    await Future.delayed(const Duration(milliseconds: 500));
    setState(() {
      animate = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image:
                DecorationImage(image: AssetImage(AppAssets.homeBackGraound))),
        child: Stack(
          children: [
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: animate ? 5.h : -5.h,
                left: animate ? 5.w : -5.w,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.blue))),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: animate ? 5.h : -5.h,
                right: animate ? 5.w : -5.w,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.yallow))),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                top: 25.h,
                left: !AppSharedPreferences.hasArLang
                    ? animate
                        ? 10.w
                        : -10.w
                    : null,
                right: AppSharedPreferences.hasArLang
                    ? animate
                        ? 10.w
                        : -10.w
                    : null,
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 1600),
                  opacity: animate ? 1 : 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome To".tr(context),
                        style: TextStyle(
                            color: AppColors.primaryColor,
                            fontSize: 20.sp,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: [
                          Text(
                            "Nanny".tr(context),
                            style: TextStyle(
                                color: AppColors.seconedaryColor,
                                fontSize: 30.sp,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Text(
                            "App".tr(context),
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                bottom: animate ? 30.h : 10.h,
                left: 8.w,
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 1600),
                  opacity: animate ? 1 : 0,
                  child: Container(
                    height: 30.h,
                    width: 40.w,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppAssets.blueNoteImage),
                            fit: BoxFit.fill)),
                  ),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                bottom: animate ? 30.h : 10.h,
                right: 8.w,
                child: AnimatedOpacity(
                  duration: const Duration(milliseconds: 1600),
                  opacity: animate ? 1 : 0,
                  child: Container(
                    height: 30.h,
                    width: 25.w,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppAssets.nannyImage),
                            fit: BoxFit.fill)),
                  ),
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 1600),
                right: animate ? 5.w : -5.w,
                bottom: animate ? 5.h : -10.h,
                child: Container(
                  height: 10.h,
                  width: 18.w,
                  decoration: const BoxDecoration(
                      image:
                          DecorationImage(image: AssetImage(AppAssets.purple))),
                )),
          ],
        ),
      ),
    );
  }
}
