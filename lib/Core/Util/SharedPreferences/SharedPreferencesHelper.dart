import 'package:kjteacher/Core/Constants/app_strings.dart';

import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static bool? initialized;
  static SharedPreferencesProvider? pref;
  static init() async {
    pref = await SharedPreferencesProvider.getInstance();
  }

  //token
  static String get getToken => pref!.read(AppStrings.token) ?? '';
  static saveToken(String value) => pref!.save(AppStrings.token, value);
  static bool get hasToken => pref!.contains(AppStrings.token);
  static  removeToken() => pref!.remove(AppStrings.token);
    //id
  static int get getId => pref!.read(AppStrings.id) ?? -1;
  static saveId(int value) => pref!.save(AppStrings.id, value);
  static bool get hasId => pref!.contains(AppStrings.id);
  static removeId() => pref!.remove(AppStrings.id);

  //lang
  static String get getArLang => pref!.read(AppStrings.ar) ?? 'en';
  static saveArLang(String value) => pref!.save(AppStrings.ar, value);
  static bool get hasArLang => pref!.contains(AppStrings.ar);
  static removeArLang() => pref!.remove(AppStrings.ar);
}
