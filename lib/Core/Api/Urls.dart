class Urls {
  static const String baseUrl = "http://10.0.2.2:8000/api/";
  static const String loginApi = "${baseUrl}login";
  static const String teachersList = "${baseUrl}teachers";
  static const String teachersProfile = "${baseUrl}teachers/";
  static const String guardians = "${baseUrl}guardians/";
  static const String foodList = "${baseUrl}food";
  static const String foodDetails = "${baseUrl}food/";
  static const String teacherCertificates = "${baseUrl}teacher-certificates/";
  static const String classRoom = "${baseUrl}classrooms/";
  static const String childDetails = "${baseUrl}children/";
    static const String order = "${baseUrl}orders";
        static const String busLines = "${baseUrl}bus-lines/";
        static const String changeState = "${baseUrl}children/";
             static const String lineSupervisors = "${baseUrl}line-supervisors/";
               static const String teachers = "${baseUrl}teachers/";
                 static const String viewOrder = "${baseUrl}orders/";
}
