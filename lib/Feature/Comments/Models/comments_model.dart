// ignore_for_file: public_member_api_docs, sort_constructors_first
class CommentsModel {
  String userName;
  String commend;
  CommentsModel({
  
    required this.userName,
    required this.commend,
  });
}

List<CommentsModel> commend = [
  CommentsModel( commend: "thx for this", userName: "Sara"),
  CommentsModel( commend: "i love you ", userName: "Samar"),
  CommentsModel( commend: "best app ", userName: "Jared"),
  CommentsModel( commend: "thx for this", userName: "Marguerite"),
  CommentsModel( commend: "thx for this", userName: "Sara")
];

List<CommentsModel> commendForNanny = [
  CommentsModel( commend: "thx for this", userName: "Sara"),
  CommentsModel( commend: "i love you ", userName: "Samar"),
  CommentsModel( commend: "you are the best ", userName: "Jared"),
  CommentsModel( commend: "good nanny", userName: "Marguerite"),
  CommentsModel( commend: "thx for this", userName: "Sara"),
  CommentsModel( commend: "best nanny", userName: "Pearl")
];

List<CommentsModel> commendForFood = [
  CommentsModel( commend: "thx for this", userName: "Sara"),
  CommentsModel( commend: "i love you ", userName: "Samar"),
  CommentsModel( commend: "best app in the word ", userName: "Jared"),
  CommentsModel( commend: "thx for every thing", userName: "Marguerite"),
  CommentsModel( commend: "best food", userName: "Sara")
];
