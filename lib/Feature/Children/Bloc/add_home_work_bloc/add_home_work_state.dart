part of 'add_home_work_bloc.dart';

@immutable
abstract class AddHomeWorkState {}

class AddHomeWorkInitial extends AddHomeWorkState {}
class SuccesAddHomeWork extends AddHomeWorkState {}
class LoadingAddHomeWork extends AddHomeWorkState {}
