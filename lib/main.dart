import 'package:flutter/material.dart';
import 'package:kjteacher/App/app.dart';
import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();
  await Network.init();
  print("ID is ${AppSharedPreferences.getId}");
  print("token is ${AppSharedPreferences.getToken}");
  print("Language is ${AppSharedPreferences.getArLang}");
  runApp(const MyApp());
}
