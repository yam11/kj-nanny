part of 'children_profile_bloc.dart';

@immutable
abstract class ChildrenProfileState {}

class ChildrenProfileInitial extends ChildrenProfileState {}

class SuccesToViewOrder extends ChildrenProfileState {}

class ErrorToViewOrder extends ChildrenProfileState {
  final String message;
  ErrorToViewOrder({required this.message});
}

class LoadingToViewOrder extends ChildrenProfileState {}

class SuccesToGetChildrenDetails extends ChildrenProfileState {}

class ErrorToGetChildrenDetails extends ChildrenProfileState {
  final String message;
  ErrorToGetChildrenDetails({required this.message});
}

class LoadingToGetChildrenDetails extends ChildrenProfileState {}
class LoadingToDeleteOrder extends ChildrenProfileState {}
class SuccesToDeleteOrder extends ChildrenProfileState {}
