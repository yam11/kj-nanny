import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:kjteacher/Core/Api/ExceptionsHandle.dart';
import 'package:kjteacher/Core/Api/Network.dart';
import 'package:kjteacher/Core/Api/Urls.dart';
import 'package:kjteacher/Feature/Main/Models/children_teacher_model.dart';
import 'package:meta/meta.dart';

part 'children_teacher_event.dart';
part 'children_teacher_state.dart';

class ChildrenTeacherBloc
    extends Bloc<ChildrenTeacherEvent, ChildrenTeacherState> {
  ChildTeacherModel? childTeacherModel;
  ChildrenTeacherBloc() : super(ChildrenTeacherInitial()) {
    on<ChildrenTeacherEvent>((event, emit) async {
      if (event is ChildrenTeacher) {
        emit(LoadingToGetChildrenTeacher());
        try {
          final response = await Network.getData(url: "${Urls.teachers}13");
          childTeacherModel = ChildTeacherModel.fromJson(response.data);
          emit(SuccesToGetChildrenTeacher());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenTeacher(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenTeacher(message: "error"));
          }
        }
      }
    });
  }
}
