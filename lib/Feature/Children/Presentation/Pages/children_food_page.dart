import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/tilte_page_widget.dart';
import 'package:kjteacher/Feature/Children/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:kjteacher/Feature/Children/Presentation/Widgets/child_food_widget.dart';
import 'package:sizer/sizer.dart';

class MyChildrenFoodPage extends StatelessWidget {
  ChildrenProfileBloc childrenProfileBloc;
  String childName;

  MyChildrenFoodPage(
      {required this.childName, required this.childrenProfileBloc, Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: Column(
          children: [
            FadeInDown(
                duration: const Duration(milliseconds: 1600),
                child: TitlePageWidget(title: "Child food".tr(context))),
            Expanded(
              child: childrenProfileBloc
                      .childDetailsModel!.data!.orders!.isNotEmpty
                  ? ListView.builder(
                      physics: const BouncingScrollPhysics(
                          parent: AlwaysScrollableScrollPhysics()),
                      itemCount: childrenProfileBloc.order.length,
                      itemBuilder: (context, index) {
                        return FadeInUpBig(
                          duration: const Duration(milliseconds: 1600),
                          child: ChildFoodWidget(
                            name: "childrenProfileBloc.order[index].",
                            decoration: "",
                            index: index,
                            // index: index,
                            // childrenProfileBloc: childrenProfileBloc,
                            // avatarImage: AppAssets.profileIcon,
                            // name: childName,
                            // totalPrice: childrenProfileBloc
                            //     .order[index].totalPrice!,
                            // orderId: childrenProfileBloc.order[index].id!,
                          ),
                        );
                      },
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 2.h, vertical: 2.w),
                          margin: EdgeInsets.symmetric(
                              horizontal: 10.w, vertical: 10.h),
                          decoration: BoxDecoration(
                              color: AppColors.fourthColor.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(5.w),
                              border: Border.all(
                                  color: AppColors.primaryColor, width: 1)),
                          child: Column(children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "The child does not have orders"
                                        .tr(context),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 15.sp,
                                        color: AppColors.seconedaryColor,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ]),
                        ),
                      ],
                    ),
            )
          ],
        ),
      )),
    );
  }
}
