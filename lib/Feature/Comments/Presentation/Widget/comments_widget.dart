import 'package:flutter/material.dart';
import 'package:kjteacher/Feature/Comments/Models/comments_model.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:sizer/sizer.dart';

class CommentsWidget extends StatelessWidget {
  String avatarImage;
  List<CommentsModel> commendModel;
  int index;
  CommentsWidget(
      {required this.index,
      required this.avatarImage,
      required this.commendModel,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 5.w,
      ),
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      child: Row(
        children: [
          Column(
            children: [
              CircleAvatar(
                radius: 8.w,
                backgroundColor: AppColors.primaryColor,
                backgroundImage: AssetImage(avatarImage),
              ),
              SizedBox(
                  width: 20.w,
                  child: Text(
                    commendModel[index].userName,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: AppColors.primaryColor,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.bold),
                  ))
            ],
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.h),
              padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 3.h),
              decoration: BoxDecoration(
                  color: AppColors.whiteColor,
                  boxShadow: [
                    BoxShadow(
                      color: const Color.fromARGB(255, 135, 152, 167)
                          .withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(5.w),
                      bottomRight: Radius.circular(5.w),
                      topRight: AppSharedPreferences.hasArLang
                          ? Radius.circular(0.w)
                          : Radius.circular(5.w),
                      topLeft: AppSharedPreferences.hasArLang
                          ? Radius.circular(5.w)
                          : Radius.circular(0.w))),
              child: Text(commendModel[index].commend,
                  style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 14.sp,
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
