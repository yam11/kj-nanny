import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjteacher/App/app_localizations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';
import 'package:kjteacher/Core/Widgets/app_bar_widget.dart';
import 'package:kjteacher/Core/Widgets/back_graound_widget.dart';
import 'package:kjteacher/Core/Widgets/error_message_widget.dart';
import 'package:kjteacher/Core/Widgets/loading_widget.dart';
import 'package:kjteacher/Feature/Children/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:kjteacher/Feature/Children/Presentation/Pages/add_homework_page.dart';
import 'package:kjteacher/Feature/Children/Presentation/Pages/add_report_page.dart';
import 'package:kjteacher/Feature/Children/Presentation/Pages/children_food_page.dart';
import 'package:kjteacher/Feature/Children/Presentation/Widgets/child_details_card_widget.dart';

import 'package:page_transition/page_transition.dart';

class ChildrenDetailsPage extends StatelessWidget {
  int classRoomId;
  int childId;
  String childName;

  ChildrenDetailsPage(
      {required this.childName,
      required this.childId,
      required this.classRoomId,
      Key? key})
      : super(key: key);
  ChildrenProfileBloc childrenProfileBloc = ChildrenProfileBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: BlocProvider(
          create: (context) =>
              childrenProfileBloc..add(ChildrenDetails(childId: childId)),
          child: BlocConsumer<ChildrenProfileBloc, ChildrenProfileState>(
            listener: (context, state) {},
            builder: (context, state) {
              if (state is ErrorToGetChildrenDetails) {
                return ErrorMessageWidget(
                    onPressed: () {
                      childrenProfileBloc
                          .add(ChildrenDetails(childId: childId));
                    },
                    message: state.message);
              }
              if (state is SuccesToGetChildrenDetails) {
                return SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        FadeInLeft(
                          duration: const Duration(seconds: 1),
                          child: ChildDetailsCardWidget(
                            cardImage: AppAssets.homeworkIcon,
                            cardTitle: "Add HomeWorks".tr(context),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: AddHomeWorkPage(
                                          childrenProfileBloc:
                                              childrenProfileBloc),
                                      type: PageTransitionType.rightToLeft,
                                      duration:
                                          const Duration(milliseconds: 300)));
                            },
                          ),
                        ),
                        FadeInRight(
                          duration: const Duration(seconds: 1),
                          child: ChildDetailsCardWidget(
                            cardImage: AppAssets.reportIcon,
                            cardTitle: "Add Reports".tr(context),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: AddReportPage(
                                        childrenProfileBloc:
                                            childrenProfileBloc,
                                      ),
                                      type: PageTransitionType.rightToLeft,
                                      duration:
                                          const Duration(milliseconds: 300)));
                            },
                          ),
                        ),
                        // FadeInLeft(
                        //   duration: const Duration(seconds: 1),
                        //   child: ChildDetailsCardWidget(
                        //     cardImage: AppAssets.resultsIcon,
                        //     cardTitle: "Absence record".tr(context),
                        //     onTap: () {
                        //       Navigator.push(
                        //           context,
                        //           PageTransition(
                        //               child: ChildrenAbsenceRecordPage(),
                        //               type: PageTransitionType.rightToLeft,
                        //               duration: const Duration(milliseconds: 300)));
                        //     },
                        //   ),
                        // ),

                        FadeInLeft(
                          duration: const Duration(seconds: 1),
                          child: ChildDetailsCardWidget(
                            cardImage: AppAssets.foodIcon,
                            cardTitle: "Child food".tr(context),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: MyChildrenFoodPage(
                                        childName: childName,
                                        childrenProfileBloc:
                                            childrenProfileBloc,
                                      ),
                                      type: PageTransitionType.rightToLeft,
                                      duration:
                                          const Duration(milliseconds: 300)));
                            },
                          ),
                        ),
                      ],
                    ));
              } else {
                return const LoadingWidget();
              }
            },
          ),
        ),
      )),
    );
  }
}
