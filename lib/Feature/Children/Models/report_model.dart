// ignore_for_file: public_member_api_docs, sort_constructors_first
class NoteModel {
  String hour;
  String note;
  NoteModel({
    required this.hour,
    required this.note,
  });
}

List<NoteModel> note = [
  NoteModel(
      hour: "2023-02-24 | 13:50:22",
      note:
          "Sint fugiat voluptatum itaque a eligendi. Odit in ut qui veniam est voluptatem. Accusantium eaque tenetur voluptatem assumenda.")
];
