import 'package:flutter/material.dart';
import 'package:kjteacher/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class LanguageButton extends StatelessWidget {
  final String title;
  final bool isPressed;
  final onPress;

  const LanguageButton(
      {Key? key,
      required this.title,
      required this.isPressed,
      required this.onPress})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
        margin: EdgeInsets.symmetric(horizontal: 2.w),
        decoration: BoxDecoration(
            border: Border.all(
                color: isPressed
                    ? AppColors.seconedaryColor
                    : AppColors.primaryColor,
                width: 2),
            borderRadius: BorderRadius.circular(5.w),
            color: AppColors.primaryColor,
            boxShadow: const [
              BoxShadow(
                color: Colors.black87,
                blurRadius: 10,
                offset: Offset(0, 3),
              )
            ]),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                color: AppColors.seconedaryColor,
                fontSize: 14.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
