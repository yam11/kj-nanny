import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:kjteacher/Core/Constants/app_assets.dart';

import 'package:kjteacher/Feature/Children/Presentation/Widgets/child_card_widget.dart';
import 'package:kjteacher/Feature/Main/Bloc/bloc/children_teacher_bloc.dart';
import 'package:page_transition/page_transition.dart';

class ChildrenPage extends StatelessWidget {
  ChildrenTeacherBloc childrenTeacherBloc;
  ChildrenPage({required this.childrenTeacherBloc, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: AnimationLimiter(
      child: ListView.builder(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        itemCount:
            childrenTeacherBloc.childTeacherModel!.data!.children!.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            delay: const Duration(milliseconds: 100),
            child: SlideAnimation(
              duration: const Duration(milliseconds: 2500),
              curve: Curves.fastLinearToSlowEaseIn,
              horizontalOffset: 30,
              verticalOffset: 350,
              child: FlipAnimation(
                duration: const Duration(milliseconds: 3000),
                curve: Curves.fastLinearToSlowEaseIn,
                flipAxis: FlipAxis.y,
                child: ChildCardWidget(
                  childId: childrenTeacherBloc
                      .childTeacherModel!.data!.children![index].id!,
                  index: index,
                  name: childrenTeacherBloc
                      .childTeacherModel!.data!.children![index].firstName!,
                  childState: childrenTeacherBloc
                      .childTeacherModel!.data!.children![index].status!,
                  childrenTeacherBloc: childrenTeacherBloc,
                  image: AppAssets.myProfileIcon,
                ),
              ),
            ),
          );
        },
      ),
    ));
  }
}
