class HomeWorkModel {
  String name;
  String decoration;

  HomeWorkModel({
    required this.name,
    required this.decoration,
  });
}

List<HomeWorkModel> homeWork = [
  HomeWorkModel(
      name: "Color",
      decoration:
          "Sint fugiat voluptatum itaque a eligendi. Odit in ut qui veniam est voluptatem. Accusantium eaque tenetur voluptatem assumenda.")
];
