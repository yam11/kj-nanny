import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:kjteacher/Feature/Children/Models/home_work_model.dart';
import 'package:meta/meta.dart';

part 'add_home_work_event.dart';
part 'add_home_work_state.dart';

class AddHomeWorkBloc extends Bloc<AddHomeWorkEvent, AddHomeWorkState> {
  TextEditingController nameController = TextEditingController();
  TextEditingController decorationController = TextEditingController();
  AddHomeWorkBloc() : super(AddHomeWorkInitial()) {
    on<AddHomeWorkEvent>((event, emit) async {
      if (event is AddHomeWork) {
        emit(LoadingAddHomeWork());
        await Future.delayed(const Duration(seconds: 1));
        homeWork.add(HomeWorkModel(
            name: nameController.text, decoration: decorationController.text));
        emit(SuccesAddHomeWork());
      }
    });
  }
}
