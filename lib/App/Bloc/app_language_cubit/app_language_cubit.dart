import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:kjteacher/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

part 'app_language_state.dart';

class AppLanguageCubit extends Cubit<ChangeLanguage> {
  AppLanguageCubit()
      : super(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));

  Future<void> getLanguage() async {
    emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
  }

  Future<void> changeLanguageToAr() async {
    await AppSharedPreferences.saveArLang('ar');
    emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
  }

  Future<void> changeLanguageToEn() async {
    await AppSharedPreferences.removeArLang();
    emit(ChangeLanguage(locale: Locale(AppSharedPreferences.getArLang)));
  }
}
